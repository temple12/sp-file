package com.file.sp.service.impl;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.file.sp.dao.MemberDAO;
import com.file.sp.service.MemberService;
import com.file.sp.vo.MemberVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MemberServiceImpl implements MemberService {
	@Autowired
	private MemberDAO mDAO;
	
	private String savePath = "C:\\Users\\Administrator\\git\\sp-file\\src\\main\\webapp\\resources\\";
	@Override
	
	@Transactional
	public int insertMember(MemberVO member) {
		MultipartFile mf = member.getFile1();
		File f = null;

		if(mf!=null) {
			String extName = mf.getOriginalFilename();
			member.setFileName(extName);
			extName = extName.substring(extName.lastIndexOf("."));
			String fileName = savePath + System.nanoTime()+extName;			
			try {
				f = new File(fileName);
				mf.transferTo(f);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		int result = mDAO.insertMember(member);
		if(result !=1 && f!=null) {
			f.delete();
		}
		return result;
	}

}
